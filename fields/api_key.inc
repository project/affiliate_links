<?php

/**
 * @file
 * API key field.
 */

/**
 * Return form element for this field.
 */
function affiliate_links_api_key_form($form_state, $account = NULL,
$settings = array()) {
  $form['api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API key'),
    '#default_value' => isset($account->fields['api_key']) ?
      $account->fields['api_key'] : '',
    '#required' => TRUE,
  );
  $form['api_key'] = $settings + $form['api_key'];
  return $form;
}

/**
 * Validate callback for affiliate_links_api_key_form().
 */
function affiliate_links_api_key_validate($form, $form_state,
$account = NULL) {

}
