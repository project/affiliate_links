<?php

/**
 * @file
 * PostAffiliatePro provider.
 */

/**
 * PostAffiliatePro provider.
 */
class AffiliateLinksPostAffiliatePro extends AffiliateLinksProvider {
  /**
   * Override AffiliateLinksProvider::getInfo().
   */
  static function getInfo() {
    return array(
      'fields' => array(
        'affiliate_id',
        array(
         'name' => 'affiliate_key',
         'form' => array(
           '#type' => 'value',
           '#value' => 'a_id',
          ),
        ),
      ),
    );
  }

}
