<?php

/**
 * @file
 * Amazon provider.
 */

/**
 * Amazon provider.
 */
class AffiliateLinksAmazon extends AffiliateLinksProvider {
  /**
   * Override AffiliateLinksProvider::getInfo().
   */
  static function getInfo() {
    return array(
      'fields' => array(
        'affiliate_id',
        array(
         'name' => 'affiliate_key',
         'form' => array(
           '#type' => 'value',
           '#value' => 'tag',
          ),
        ),
      ),
    );
  }

}
