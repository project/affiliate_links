<?php

/**
 * @file
 * AHAH callback handlers.
 */

/**
 * Menu callback; Update provider required fields via AHAH.
 */
function affiliate_links_callback($element, $file = 'admin') {
  affiliate_links_include($file);
  $form_state = array('storage' => NULL, 'submitted' => FALSE);
  $form_build_id = $_POST['form_build_id'];
  $form = form_get_cache($form_build_id, $form_state);

  $args = $form['#parameters'];
  $form_id = array_shift($args);
  $form_state['post'] = $form['#post'] = $_POST;
  $form['#programmed'] = $form['#redirect'] = FALSE;
  _affiliate_links_disable_validation($form);

  drupal_process_form($form_id, $form, $form_state);
  $form = drupal_rebuild_form($form_id, $form_state, $args, $form_build_id);

  $choice_form = $form[$element];
  unset($choice_form['#prefix'], $choice_form['#suffix']);
  $output = theme('status_messages') . drupal_render($choice_form);

  // Final rendering callback.
  drupal_json(array('status' => TRUE, 'data' => $output));
}

/**
 * Disable validation for all of the form elements.
 */
function _affiliate_links_disable_validation(&$form) {
  foreach (element_children($form) as $child) {
    $form[$child]['#validated'] = TRUE;
    $form[$child]['#element_validate'] = NULL;
    $form[$child]['#needs_validation'] = FALSE;
    _affiliate_links_disable_validation($form[$child]);
  }
}
