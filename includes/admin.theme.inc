<?php

/**
 * @file
 * Theme functions for admin pages.
 */

/**
 * Theme provider accounts page.
 */
function theme_affiliate_links_account_page($form) {
  $has_account = isset($form['name']) && is_array($form['name']);
  $output = '';

  $header = array(
    $has_account ? theme('table_select_header_cell') : '',
    array('data' => t('Account'), 'field' => 'name', 'sort' => 'asc'),
    array('data' => t('Provider'), 'field' => 'provider'),
    array('data' => t('Cloaked'), 'field' => 'cloaked'),
    array('data' => t('Operations')),
  );
  $rows = array();
  if ($has_account) {
    foreach (element_children($form['name']) as $key) {
      $rows[] = array(
        drupal_render($form['checkboxes'][$key]),
        drupal_render($form['name'][$key]),
        drupal_render($form['provider'][$key]),
        drupal_render($form['cloaked'][$key]),
        drupal_render($form['edit'][$key]),
      );
    }
  }
  else {
    $rows[] = array(
      array(
        'data' => '<em>' . t('No provider accounts set up yet.') . '</em>',
        'colspan' => 5,
      ),
    );
  }
  $output .= theme('table', $header, $rows);
  $output .= drupal_render($form['pager']);

  $output .= drupal_render($form);
  return $output;
}

/**
 * Theme cloaked link report page.
 */
function theme_affiliate_links_report_page($form) {
  $has_link = isset($form['lid']) && is_array($form['lid']);
  $output = '';

  $header = array(
    array('data' => t('Account'), 'field' => 'a.name'),
    array('data' => t('Provider'), 'field' => 'a.provider'),
    array('data' => t('Cloaked link'), 'field' => 'l.lid'),
    array('data' => t('Source'), 'field' => 'l.source'),
    array('data' => t('Destination'), 'field' => 'l.dest'),
    array('data' => t('Count'), 'field' => 'l.count', 'sort' => 'desc'),
  );
  $rows = array();
  if ($has_link) {
    foreach (element_children($form['name']) as $key) {
      $rows[] = array(
        drupal_render($form['name'][$key]),
        drupal_render($form['provider'][$key]),
        drupal_render($form['lid'][$key]),
        drupal_render($form['source'][$key]),
        drupal_render($form['dest'][$key]),
        drupal_render($form['count'][$key]),
      );
    }
  }
  else {
    $rows[] = array(
      array(
        'data' => '<em>' . t('No cloaked links generated yet.') . '</em>',
        'colspan' => 6,
      ),
    );
  }
  $output .= theme('table', $header, $rows);
  $output .= drupal_render($form['pager']);

  $output .= drupal_render($form);
  return $output;
}
