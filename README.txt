Affiliate Links
---------------

This module handles outgoing links to various affiliate networks.


INSTALLATION
------------

Install Affiliate Links module as usual, see http://drupal.org/node/70151 for
more information.


CONFIGURATION
-------------

Configure the module settings at:
  Administer > Site building > Affiliate links

Check cloaked link statistic at:
  Administer > Reports > Affiliate links statistic


CONTACT
-------

Sam Men Wee (mwsam)
http://drupal.org/user/691740
